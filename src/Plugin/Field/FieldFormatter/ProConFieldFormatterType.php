<?php

namespace Drupal\procon_field\Plugin\Field\FieldFormatter;

use Drupal;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'procon_field_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "procon_field_formatter_type",
 *   label = @Translation("Pros and Cons field formatter type"),
 *   field_types = {
 *     "procon_field_type"
 *   }
 * )
 */
class ProConFieldFormatterType extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        // Implement default settings.
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
        // Implement settings form.
      ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $pros = [];
    $cons = [];

    foreach ($items as $delta => $item) {
      $type = $item->get('procon_type')->getCastedValue();
      switch ($type) {

        case 'pro':
          $pros[$delta] = $this->viewValue($item);
          break;

        case 'con':
          $cons[$delta] = $this->viewValue($item);
          break;
      }
    }


    $elements['pros'] = [
      '#theme' => 'item_list',
      '#title' => $this->t('Pros'),
      '#empty' => $this->t('None'),
      '#items' => $pros,
      '#attributes' => [
        'class' => ['prcon-field--pro'],
      ],
    ];

    $elements['cons'] = [
      '#theme' => 'item_list',
      '#title' => $this->t('Cons'),
      '#empty' => $this->t('None'),
      '#items' => $cons,
      '#attributes' => [
        'class' => ['prcon-field--con'],
      ],
    ];

    return ['#markup' => Drupal::service('renderer')->render($elements)];
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
